(*i camlp4deps: "parsing/grammar.cma" i*)

open Ltac_plugin
open Stdarg

DECLARE PLUGIN "trivial"

TACTIC EXTEND disp_tactic
| [ "disp" ] -> [ Proofview.Goal.nf_enter (fun gl -> Debug.print_goal gl; Tacticals.New.tclIDTAC) ]
END;;

VERNAC COMMAND EXTEND Disp_command CLASSIFIED AS QUERY
| [ "Disp" ] -> [ Debug.print_current () ]
END;;

VERNAC COMMAND EXTEND Dispglobal_command CLASSIFIED AS QUERY
| [ "DispGlobal" ] -> [ Debug.print_global () ]
END;;

VERNAC COMMAND EXTEND Define_command CLASSIFIED AS SIDEFF
| [ "Test" ] -> [ Inject.test_inject () ]
END;;

VERNAC COMMAND EXTEND Inject_command CLASSIFIED AS SIDEFF
| [ "Inject" constr(c) ] -> [ Inject.definition_inject_constrexpr c ]
END;;

VERNAC COMMAND EXTEND Inject_evar_command CLASSIFIED AS SIDEFF
| [ "InjectEvar" ] -> [ Inject.evar_inject () ]
END;;
