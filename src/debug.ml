let print_header title =
    Feedback.msg_info Pp.(str "");
    Feedback.msg_info Pp.(str "<< " ++ str title ++ str " >>");
    Feedback.msg_info Pp.(str "")

let print_frame title =
    Feedback.msg_info Pp.(str (String.make 80 '-'));
    Feedback.msg_info Pp.(str "# " ++ str title);
    Feedback.msg_info Pp.(str "")

let print_frame_end () =
    Feedback.msg_info Pp.(str "")

let print_evar evar_map env evar_id evar_info =
    let name = match (Evd.evar_ident evar_id evar_map) with
        | None -> "anonymous"
        | Some id -> Names.Id.to_string id
    in
    let name = Printf.sprintf "%3d | %12s : " (Evar.repr evar_id) name in
    Feedback.msg_info Pp.(
        str name ++
        Printer.pr_constr_env env evar_map (Evd.evar_concl evar_info) ++
        str " " ++
        match (Evd.evar_body evar_info) with
        | Evd.Evar_empty -> str "(undefined yet)"
        | Evd.Evar_defined c -> str ":= " ++ Printer.pr_constr_env env evar_map c
    );  
    evar_info

let print_evar_map evar_map env =
    print_frame "evar_map (sigma)";
    let _ = Evd.raw_map (print_evar evar_map env) evar_map in
    print_frame_end ()

let print_env sigma env =
    print_frame "env";
    Feedback.msg_info Pp.(str "< rels " ++ int (Environ.nb_rel env) ++ str " in total >");
    Feedback.msg_info Pp.(str "constants: \n");
    let count = ref 0 in
    Environ.fold_named_context begin fun env decl () ->
        count := !count + 1;
        let name = Names.Id.to_string (Context.Named.Declaration.get_id decl) in
        let name = Printf.sprintf "%3d | %12s" !count name in
        let _type = Context.Named.Declaration.get_type decl in
        let _value = Context.Named.Declaration.get_value decl in
        Feedback.msg_info Pp.(
            str name ++
            str " : " ++
            Printer.pr_type_env env sigma _type ++
            match _value with
            | Some c -> str " := " ++ Printer.pr_constr_env env sigma c
            | None -> str " (undefined yet)"
        );
        ()
    end env ~init:();
    (* universes of env *)
    let ugraph = Environ.universes env in
    ()

let print_global () =
    print_header "global";
    let (sigma, env) = Pfedit.get_current_context () in begin
        (* global constants? *)
        Feedback.msg_info Pp.(str "global constants: \n");
        let open Pre_env in
        let global = (Environ.pre_env env).env_globals in begin
            Names.Cmap_env.iter begin fun key const ->
                Feedback.msg_info (Names.Constant.debug_print key);
                Feedback.msg_info Printer.(pr_constant env key);
            end global.env_constants
        end;
    end;
    print_frame_end ()

let print_current () =
    print_header "current context";
    let (sigma, env) = Pfedit.get_current_context () in
    begin
        print_evar_map sigma env;
        print_env sigma env;
    end;
    ()

let print_goal gl =
    print_header "goal context";
    Feedback.msg_info (Pp.str (String.make 80 '-'));
    Feedback.msg_info Pp.(str "Goal: " ++ Printer.pr_econstr_env (Proofview.Goal.env gl) (Proofview.Goal.sigma gl) (Proofview.Goal.concl gl));
    print_evar_map (Proofview.Goal.sigma gl) (Proofview.Goal.env gl);
    print_env (Proofview.Goal.sigma gl) (Proofview.Goal.env gl);
    ()
