open Constr
open Constrexpr

exception Unhandled_transformation of string

let rec constrexpr_of_constr sigma env c =
    let c = Term.kind_of_term c in
    let constrexpr_r_of_constr c =
        match c with
        | Rel _                 ->  raise   (Unhandled_transformation "rel")
        | Var id                ->  CRef    (Libnames.Ident (None, id), None)
        | Meta _                ->  raise   (Unhandled_transformation "meta")
        | Evar _                ->  raise   (Unhandled_transformation "evar")
        | Sort _                ->  raise   (Unhandled_transformation "sort")
        | Cast _                ->  raise   (Unhandled_transformation "cast")
        | Prod _                ->  raise   (Unhandled_transformation "prod")
        | Lambda _              ->  raise   (Unhandled_transformation "lambda")
        | LetIn _               ->  raise   (Unhandled_transformation "let-in")
        | App (f, args)         ->  CApp    (
                                                (None, constrexpr_of_constr sigma env f),
                                                List.map begin fun c -> (constrexpr_of_constr sigma env c, None) end (Array.to_list args)
                                            )
        | Const _               ->  raise   (Unhandled_transformation "const")
        | Ind _                 ->  raise   (Unhandled_transformation "ind")
        | Construct (c, univ)   ->  let (ind, cindex) = c in 
                                    let (mind, index) = ind in
                                    Feedback.msg_info Printer.(pr_constructor env c);
                                    raise   (Unhandled_transformation "construct")
        | Case _                ->  raise   (Unhandled_transformation "case")
        | Fix _                 ->  raise   (Unhandled_transformation "fix")
        | CoFix _               ->  raise   (Unhandled_transformation "cofix")
        | Proj  _               ->  raise   (Unhandled_transformation "proj")
    in
        CAst.make (constrexpr_r_of_constr c)
