let mk_definition name typ value =
    Command.do_definition
        (Names.Id.of_string name)
        (Decl_kinds.Global, false, Decl_kinds.Definition)
        None
        List.[]
        None
        (* value : Constrexpr *)
        value
        None
        (* an empty hook *)
        (Lemmas.mk_hook begin fun _ _ -> () end)

let test_inject () =
    mk_definition "test_a" None (CAst.make (Constrexpr.CPrim (Constrexpr.Numeral ("0", false))));
    mk_definition "test_b" None (CAst.make (Constrexpr.CRef (Libnames.Ident ((None, Names.Id.of_string "O")), None)))

let definition_inject_constrexpr =
    let count = ref 0 in
    fun c ->
        mk_definition Printf.(sprintf "test_%d" !count) None c;
        count := !count + 1;
        ()

let evar_inject () =
    let (sigma, env) = Pfedit.get_current_context () in
    let single_evar_inject = begin
        fun evar evar_info _ ->
            let evar_type = Printer.(pr_constr_env env sigma (Evd.evar_concl evar_info)) in
            match Evd.evar_body evar_info with
            | Evd.Evar_empty -> Feedback.msg_info Pp.(evar_type ++ str " (undefined yet)")
            | Evd.Evar_defined c -> begin
                try
                    let ce = Transform.constrexpr_of_constr sigma env c in
                    definition_inject_constrexpr ce
                with Transform.Unhandled_transformation msg ->
                    Feedback.msg_info Pp.(evar_type ++ str " (failed because " ++ str msg ++ str ")")
            end
    end in
    Evd.fold single_evar_inject sigma ()
