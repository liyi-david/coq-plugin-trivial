TODO LOGS
===

Jul 26th
---

- What is evar exactly?
> Evar is an abbrivation of **existential-variables**, refer to
> https://coq.inria.fr/refman/language/gallina-extensions.html#existential-variables
> For simple cases, e.g. `Goal 1 > 0.`, applying `evar (H:1>0).` and `assert (H:1>0).`
> leads to exactly the same results.
- Where are the global constants and how to obtain them from an environment?
> First it is not contained in named_context of `Global.env ()`.
- Figure out how a tactic is applied.
- Difference between `eConstr` and `constr`.
- Where are the proof terms stored?
- How can we add a new hypothesis/variable/... on the top level?
> 1. `save_proof` in proof/lemmas.ml provides a solution where proved goals are stored as theorems. this still requires further exploration. **TODO**
> 2. `vernac/comDefinition.ml` provides a solution to define items on top-level.

Jul 27th
---

- Generate declarations and definitions and add them to global environment.
> `vernac/comDefinition` provides an API named `do_definition` that can add various types of definitions to local/global environment.
> In the new vernac command `Test`, a constant natural number is successfully added.
> However, it is still a problem how to convert `constr` to `constr_expr` when defining coq items.
- Load constants from global environment.
> `kernel/Pre_env` provides an exposed type definition of environment, through which we can obtain the global constants as a map.

Aug 2nc
---

- From constructor to CPrim?

NOTES
===

Concepts
---
My own understanding of some concepts are listed here. Some of them may not be quite accurate and need further exploration.

- *context* Contexts are local environments containing constants, declarations, etc.  

    - Context is a local concepts, in other words, a coq item may belongs to no context, in this case it is a global item.
    - Contexts are created when (1) entering a new section and (2) proposing a new goal. When a context is created, it inherits all items in the current context (if there is a current context).

- *evar*
- *sigma* a map of *evar*, also called *evar_map* in some cases,
- *env*
  - env_named_context : section variables (hypothesis, etc.)
- *pre_env* Somehow in Coq*env*

MARKED MATERIALS
===

Coq
---
- http://www.cs.cornell.edu/courses/cs3110/2018sp/a5/coq-tactics-cheatsheet.html
- universes: https://coq.inria.fr/refman/addendum/universe-polymorphism.html
